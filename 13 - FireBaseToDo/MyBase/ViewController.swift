//
//  ViewController.swift
//  MyBase
//
//  Created by Daniel Macedo on 08/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBAction func login(_ sender: UIButton) {
        FIRAuth.auth()?.signIn(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
            
            
            print("Signed \(error?.localizedDescription)")
        }
    }
    
    @IBAction func signup(_ sender: UIButton) {
        FIRAuth.auth()?.createUser(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
            
            print("Created \(error?.localizedDescription)")
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FIRAuth.auth()?.addStateDidChangeListener() { (auth, user) in
            if user != nil {
                print("Logged In \(user?.email)")
                self.performSegue(withIdentifier: "GroupToDoList", sender: nil)
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

