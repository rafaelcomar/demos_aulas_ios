//
//  Item.swift
//  MyBase
//
//  Created by Daniel Macedo on 08/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import Foundation
import Firebase

struct Item {
    var title:String?
    var addedBy:String?
    var completed:Bool
    var ref: FIRDatabaseReference?
    func toAnyObject() -> Any {
        return ["title":title!, "addedBy":addedBy!, "completed":completed]
    }
}
