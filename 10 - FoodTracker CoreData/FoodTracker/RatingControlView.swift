//
//  RatingControlView.swift
//  FoodTracker
//
//  Created by Daniel Macedo on 02/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class RatingControlView: UIView {
    
    var ratingButtons = [UIButton]()

    private var _rating = 0
    
    var rating:Int {
        get {
            return _rating
        }
        
        set {
            _rating = newValue
            self.updateRatingButtons()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        for _ in 0..<5 {
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            let emptyStar = UIImage(named: "emptyStar")
            let filledStar = UIImage(named: "filledStar")
            
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(filledStar, for: .highlighted)
            button.setImage(filledStar, for: [.highlighted, .selected])
            
            button.addTarget(self, action: #selector(RatingControlView.rate(_:)), for: UIControlEvents.touchDown)
            
            self.ratingButtons.append(button)
            self.addSubview(button)
        }
    }
    
    override func layoutSubviews() {
        let buttonSize = Int(self.frame.height)
        
        for (index, button) in self.ratingButtons.enumerated() {
            button.frame = CGRect(x: (index * (buttonSize + 8)),
                                  y: 0,
                                  width: buttonSize,
                                  height: buttonSize)
        }
    }
    
    @IBAction func rate(_ sender: UIButton) {
        self._rating = self.ratingButtons.index(of: sender)! + 1
        
        self.updateRatingButtons()
    }
    
    func updateRatingButtons() {
        for (index, button) in self.ratingButtons.enumerated() {
            if(index < self._rating) {
                button.isSelected = true
            } else {
                button.isSelected = false
            }
        }
    }
}
