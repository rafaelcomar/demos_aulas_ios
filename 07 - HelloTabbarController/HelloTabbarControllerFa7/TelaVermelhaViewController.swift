//
//  TelaVermelhaViewController.swift
//  HelloTabbarControllerFa7
//
//  Created by Daniel Macedo on 26/11/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class TelaVermelhaViewController: UIViewController {

    @IBAction func testNotification(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("Test"), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
