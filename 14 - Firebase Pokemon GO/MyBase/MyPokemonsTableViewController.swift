//
//  MyPokemonsTableViewController.swift
//  MyBase
//
//  Created by Daniel Macedo on 10/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit
import Firebase
import MapKit
class MyPokemonsTableViewController: UITableViewController {

    private var user:FIRUser?
    
    var ref: FIRDatabaseReference!
    var refUsers: FIRDatabaseReference!
    
    var pokemons:[Pokemon] = [Pokemon]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = FIRAuth.auth()?.currentUser {
            self.user = user
            print("Welcome \(self.user!.email)")
            ref = FIRDatabase.database().reference()
            refUsers = ref.child("users")
        }
        
        self.setupUserWatcher()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    func setupUserWatcher() -> Void {
        self.refUsers.child(self.user!.uid).observeSingleEvent(of:.value, with: { (snapshot) in
            self.pokemons.removeAll()
            print("- Start -")
            for childSnapshot in snapshot.children {
                let child = childSnapshot as! FIRDataSnapshot
                let value = child.value as! [String:Any]
                
                let pokemonId = value["id"] as! Int
                let pokemonName = value["name"] as! String
                let pokemon = Pokemon(name: pokemonName, coordinate: CLLocationCoordinate2DMake(0.0, 0.0))
                pokemon.id = pokemonId
                pokemon.ref = child.ref
                
                self.pokemons.append(pokemon)
                
            }
            print("- End -")
            self.tableView.reloadData()
            self.navigationItem.rightBarButtonItem?.title = "\(self.pokemons.count)"
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.pokemons.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PokemonCell", for: indexPath)

        let pokemon = self.pokemons[indexPath.row]
        cell.imageView?.image = UIImage(named: "\(pokemon.id)")
        cell.textLabel?.text = pokemon.name

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
